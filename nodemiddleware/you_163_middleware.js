/**
 *  nodeJs 中间件
 */
var express = require("express");
var app= express();
//解决跨域问题， 网上成熟的解决方案，我是直接拷贝的
app.all("*",function(req,res,next){
    res.header("ACCESS-Control-Allow-Origin","*");
    res.header("ACCESS-Control-Allow-Headers","X-Requested-Width");
    res.header("ACCESS-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",'3.2.1');
    res.header("Content-Type","application/json;charset=utf-8");
    next();
});
//获取可拖动的按钮列表接口
app.get('/get_tabbtn_List',function(req,res){
    var tabBtnList=['推荐1','居家生活','服装鞋帽','美事酒水','个护清洁','母婴亲子','全球特色'];
    res.send(tabBtnList);
});
app.listen(5632,function(){
    console.log('5632,网易严选中间件，已经运行');
})