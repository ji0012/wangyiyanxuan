
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import  './assets/rem'
import  './assets/reset.css'

import Vue from 'vue'
import App from './App'
import router from './router'
import { Col, Row,Search,Tab, Tabs, Swipe, SwipeItem,Grid, GridItem,Tabbar, TabbarItem,Sticky,Button } from 'vant';
Vue.config.productionTip = false

/* eslint-disable no-new */

Vue.use(Col).use(Row).use(Search).use(Tab).use(Tabs).use(Swipe).use(SwipeItem).use(Grid).use(GridItem).use(Tabbar).use(TabbarItem).use(Sticky).use(Button);



new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
